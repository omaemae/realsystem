import { useEffect, useState } from "preact/hooks"

const defaultRegistry = {
	System: {
		Setup: {
			Done: false,
			Stage: 0,
		},
		Users: [],
		UserData: {},
		FileAssociations: {
			Extensions: {},
		},
	},
	Network: {
		MAC: null,
	},
}

export const registryInit = () => {
	if (!localStorage["registry"]) {
		RealSystem.kernel.log("Initializing registry for the first time...")
		localStorage["registry"] = JSON.stringify(defaultRegistry)
	} else {
		RealSystem.kernel.log("Updating registry...")
		const registry = JSON.parse(localStorage["registry"])
		registryMerge(registry, defaultRegistry)
		localStorage["registry"] = JSON.stringify(registry)
	}
}

const registryMerge = (root: Record<string, unknown>, defaultRoot: Record<string, unknown>, prevKey?: string) => {
	for (const key of Object.keys(defaultRoot)) {
		if (!Object.keys(root).includes(key)) {
			RealSystem.kernel.log(`REGISTRY MERGE: ${prevKey || "Registry Root"} does not contain key ${key}, restoring from defaults.`)
			root[key] = defaultRoot[key]
		} else {
			if (typeof root[key] === "object") {
				registryMerge(
					root[key] as Record<string, unknown>,
					defaultRoot[key] as Record<string, unknown>,
					prevKey ? `${prevKey}/${key}` : key
				)
			}
		}
	}
}

const registryRead = <T>(path: string): T => {
	const registry = JSON.parse(localStorage["registry"])
	let root = registry
	const segments = path.split("/")
	const property = segments.pop()
	if (!property) throw new Error("Invalid registry path.")
	for (const segment of segments) {
		root = root[segment]
		if (root === undefined) throw new Error("Registry path does not exist.")
	}
	return root[property]
}

class RegistryUpdateEvent extends Event {
	path: string

	constructor(type: string, path: string) {
		super(type)
		this.path = path
	}
}

const registryWrite = <T>(path: string, value: T) => {
	const registry = JSON.parse(localStorage["registry"])
	let root = registry
	const segments = path.split("/")
	const property = segments.pop()
	if (!property) throw new Error("Invalid registry path.")
	for (const segment of segments) {
		const prevRoot = root
		root = root[segment]
		if (root === undefined) {
			prevRoot[segment] = {}
			root = prevRoot[segment]
		}
	}
	root[property] = value
	localStorage["registry"] = JSON.stringify(registry)
	window.dispatchEvent(new RegistryUpdateEvent("realsystem.registry.update", path))
}

const useRegistryState = <T>(path: string, defaultValue?: T): [T, (newValue: T) => void] => {
	let initialValue
	try {
		// try to read initial value from registry
		initialValue = registryRead<T>(path)
		if (initialValue === undefined) {
			// if the value is undefined, use and write default value
			initialValue = defaultValue
			registryWrite<T | undefined>(path, defaultValue as T)
		}
	} catch {
		// if that fails, use default value
		initialValue = defaultValue
		registryWrite<T | undefined>(path, defaultValue)
	}
	const [value, setValue] = useState(initialValue)
	useEffect(() => {
		const listener = (event: RegistryUpdateEvent) => {
			if (event.path === path) setValue(registryRead(path))
		}
		window.addEventListener("realsystem.registry.update", listener as (event: Event) => void)
		return () => {
			window.removeEventListener("realsystem.registry.update", listener as (event: Event) => void)
		}
	}, [])

	return [value as T, (newValue: T) => registryWrite(path, newValue)]
}

export default {
	registryRead,
	registryWrite,
	useRegistryState,
}

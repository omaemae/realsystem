import classNames from "classnames"
import { JSX } from "preact"

type InputProps = {
	className?: string
} & JSX.HTMLAttributes<HTMLInputElement>

export const Input = ({ className, ...props }: InputProps) => (
	<input
		className={classNames(
			"flex gap-2 border border-black/10 px-6 py-2 transition-all disabled:opacity-70 [&>svg]:h-6 [&>svg]:w-6",
			className
		)}
		{...props}
	/>
)

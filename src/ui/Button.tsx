import classNames from "classnames"
import { JSX } from "preact"

type ButtonProps = {
	className?: string
} & JSX.HTMLAttributes<HTMLButtonElement>

export const Button = ({ className, ...props }: ButtonProps) => (
	<button
		className={classNames(
			"flex justify-center gap-2 border border-black/5 bg-slate-200 px-6 py-2 font-semibold transition-all hover:bg-slate-50 disabled:opacity-70 [&>svg]:h-6 [&>svg]:w-6",
			className
		)}
		{...props}
	/>
)

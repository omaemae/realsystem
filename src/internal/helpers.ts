export const pascalCaseToSnakeCase = (s: string) =>
	s
		.replace(/(?:^|\.?)([A-Z])/g, function (_, y) {
			return "_" + y.toLowerCase()
		})
		.replace(/^_/, "")
		.toUpperCase()

export const readFileAsBlob = async (path: string) => {
	const buffer = await RealSystem.kernel.read(path)
	const blob = new Blob([buffer])
	return URL.createObjectURL(blob)
}

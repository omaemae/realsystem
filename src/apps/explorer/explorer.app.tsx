import { Application } from "@/kernel/app"
import { useEffect, useState } from "preact/hooks"
import styles from "@/apps/explorer/styles.module.css"
import { DocumentIcon, FolderIcon, FolderMinusIcon, FolderPlusIcon } from "@heroicons/react/24/solid"

const ExplorerWindow = () => {
	const [cwd, setCWD] = useState("/")
	const [files, setFiles] = useState<{ files: string[]; folders: string[] }>()
	const [folderName, setFolderName] = useState("")

	useEffect(() => {
		;(async () => {
			const allFiles = await RealSystem.kernel.readdir(cwd)
			const isFile = await Promise.all(allFiles.map((file) => RealSystem.kernel.isfile(RealSystem.kernel.join(cwd, file))))
			const files = allFiles.filter((_, i) => isFile[i])
			const folders = allFiles.filter((_, i) => !isFile[i])
			if (cwd != "/") folders.push("..")
			setFiles({ files, folders })
		})()
	}, [cwd])

	return (
		<div className={styles.window}>
			{/* <div className={styles.sidebar}></div> */}
			<div className={styles.main}>
				<div className={styles.topbar}>
					<p>{cwd}</p>
				</div>
				<div className={styles.folder}>
					{!files && <p>Loading...</p>}
					{files?.folders.map((folder) => (
						<button onClick={() => setCWD(RealSystem.kernel.join(cwd, folder))}>
							<FolderIcon className="h-6 w-6" /> {folder}
						</button>
					))}
					{files?.files.map((file) => (
						<button onClick={() => RealSystem.kernel.open(RealSystem.kernel.join(cwd, file))}>
							<DocumentIcon className="h-6 w-6" /> {file}
						</button>
					))}
				</div>
				<div className={styles.actionbar}>
					<form
						onSubmit={(e) => {
							e.preventDefault()
							if (!folderName) return
							RealSystem.kernel.mkdir(RealSystem.kernel.join(cwd, folderName)).then(() => {
								setCWD(RealSystem.kernel.join(cwd, folderName))
								setFolderName("")
							})
						}}
					>
						<input type="text" value={folderName} onChange={(e) => setFolderName(e.currentTarget.value)} />
						<button type="submit">
							<FolderPlusIcon /> New Folder
						</button>
					</form>
					{cwd != "/" && (
						<button
							onClick={() => {
								RealSystem.kernel.delete(cwd).then(() => setCWD(RealSystem.kernel.join(cwd, "..")))
							}}
						>
							<FolderMinusIcon /> Delete Folder
						</button>
					)}
				</div>
			</div>
		</div>
	)
}

class ExplorerApp implements Application {
	name = "Explorer"
	executable = "explorer"
	author = "omame"

	main() {
		RealSystem.shell.openWindow({
			title: "Explorer",
			children: <ExplorerWindow />,
			width: 700,
			height: 500,
		})
	}
}

export default ExplorerApp

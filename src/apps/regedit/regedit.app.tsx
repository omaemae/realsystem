import { Application } from "@/kernel/app"
import { useEffect, useState } from "preact/hooks"
import styles from "@/apps/regedit/styles.module.css"
import { ChevronDownIcon, ChevronUpIcon } from "@heroicons/react/20/solid"
import classNames from "classnames"

const SidebarObject = (props: {
	root: Record<string, unknown>
	name: string
	i: number
	path?: string
	onClick: (path: string) => void
	currentPath: string
}) => {
	const [collapsed, setCollapsed] = useState(false)

	return (
		<>
			<button style={{ paddingLeft: `${props.i}rem` }} onClick={() => props.onClick(props.path || "")}>
				<span
					className={classNames(
						(props.currentPath === props.path || (props.path === undefined && props.currentPath == "")) && styles.current,
						"transition-all"
					)}
				>
					<button onClick={() => setCollapsed(!collapsed)}>{collapsed ? <ChevronUpIcon /> : <ChevronDownIcon />}</button>
					{props.name}
				</span>
			</button>
			{!collapsed &&
				Object.keys(props.root)
					.filter((key) => typeof props.root[key] === "object")
					.map((key) => (
						<SidebarObject
							key={key}
							root={props.root[key] as Record<string, unknown>}
							name={key}
							i={props.i + 1}
							path={props.i == 0 ? key : props.path + "/" + key}
							onClick={props.onClick}
							currentPath={props.currentPath}
						/>
					))}
		</>
	)
}

const RegistryEditorWindow = () => {
	const [registry, setRegistry] = useState(JSON.parse(localStorage["registry"]))
	const [key, setKey] = useState("")

	useEffect(() => {
		const listener = () => {
			setRegistry(JSON.parse(localStorage["registry"]))
		}
		window.addEventListener("realsystem.registry.update", listener)
		return () => {
			window.removeEventListener("realsystem.registry.update", listener)
		}
	}, [])

	const root = key != "" ? RealSystem.user.registryRead(key) : registry

	return (
		<div className={styles.window}>
			<div className={styles.sidebar}>
				<SidebarObject root={registry} name="Registry Root" i={0} onClick={(path) => setKey(path)} currentPath={key} />
			</div>
			<div className={styles.keys}>
				{Object.keys(root).map((key) => (
					<p>
						<span className={styles.key}>{key}</span>
						<span className={styles.type}>({typeof root[key]})</span>
						{typeof root[key] != "object" && <span>{JSON.stringify(root[key])}</span>}
					</p>
				))}
			</div>
		</div>
	)
}

class RegistryEditorApp implements Application {
	name = "Registry Editor"
	executable = "regedit"
	author = "omame"

	main() {
		RealSystem.shell.openWindow({
			title: "Registry Editor",
			children: <RegistryEditorWindow />,
			width: 700,
			height: 500,
		})
	}
}

export default RegistryEditorApp

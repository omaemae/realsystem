import { Prompt } from "@/apps/cmd/Prompt"
import { Application } from "@/kernel/app"

class CommandPromptApp implements Application {
	name = "Command Prompt"
	executable = "cmd"
	author = "omame"

	main() {
		RealSystem.shell.openWindow({
			title: this.name,
			children: <Prompt />,
			width: 700,
			height: 500,
		})
	}
}

export default CommandPromptApp

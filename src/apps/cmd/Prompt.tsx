import { useEffect, useRef } from "preact/hooks"
import { Readline } from "xterm-readline"
import { Terminal } from "xterm"
import { FitAddon } from "xterm-addon-fit"
import { WebglAddon } from "xterm-addon-webgl"
import "xterm/css/xterm.css"
import { rsh } from "@/apps/cmd/rsh"

export const Prompt = () => {
	const terminal = useRef(
		new Terminal({
			fontFamily: "'JetBrains Mono', monospace",
			fontSize: 14,
			allowProposedApi: true,
		})
	)
	const fitAddon = useRef(new FitAddon())
	const readline = useRef(new Readline())
	const terminalDiv = useRef<HTMLDivElement>(null)

	useEffect(() => {
		if (terminalDiv.current) {
			terminal.current.open(terminalDiv.current)
			terminal.current.loadAddon(fitAddon.current)
			terminal.current.loadAddon(new WebglAddon())
			terminal.current.loadAddon(readline.current)
			fitAddon.current.fit()
			rsh({ terminal: terminal.current, readline: readline.current, cwd: `/home/${RealSystem.kernel.getUser()}` })
		}
	}, [terminalDiv])

	return (
		<div className="flex h-full bg-black p-4 text-white">
			<div className="w-full" ref={terminalDiv}></div>
		</div>
	)
}

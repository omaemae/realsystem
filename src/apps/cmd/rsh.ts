import { Terminal } from "xterm"
import { Readline } from "xterm-readline"
import { split } from "shlex"

type rshProps = {
	terminal: Terminal
	readline: Readline
} & rshState

type rshState = {
	cwd: string
}

const COLORS = {
	BLUE: "\x1b[1;34m",
	RESET: "\x1b[0m",
}

type rshBuiltin = (props: rshProps, args: string[], funcs: { cd: (path: string) => void }) => Promise<void>

const builtinCommands: Record<string, rshBuiltin> = {
	ls: async (props: rshProps, args: string[]) => {
		let dir = args.length ? args[0] : props.cwd
		if (!dir.startsWith("/")) {
			dir = RealSystem.kernel.join(props.cwd, dir)
		}
		if (!(await RealSystem.kernel.exists(dir))) {
			props.terminal.writeln("ls: directory does not exist")
			return
		}
		if (!(await RealSystem.kernel.isdir(dir))) {
			props.terminal.writeln("ls: path is not a directory")
			return
		}
		const dirContent = await RealSystem.kernel.readdir(dir)
		const isdir = await Promise.all(dirContent.map((file) => RealSystem.kernel.isdir(RealSystem.kernel.join(dir, file))))
		for (const file of dirContent) {
			const isDirectory = isdir[dirContent.indexOf(file)]
			if (isDirectory) props.terminal.write(COLORS.BLUE)
			props.terminal.write(file)
			if (isDirectory) props.terminal.write(COLORS.RESET)
			props.terminal.write("\r\n")
		}
	},
	cat: async (props: rshProps, args: string[]) => {
		if (!args.length) {
			props.terminal.writeln("cat: you must specify a file")
			return
		}
		let file = args[0]
		if (!file.startsWith("/")) {
			file = RealSystem.kernel.join(props.cwd, file)
		}
		if (!(await RealSystem.kernel.exists(file))) {
			props.terminal.writeln("cat: file does not exist")
			return
		}
		if (!(await RealSystem.kernel.isfile(file))) {
			props.terminal.writeln("cat: path is not a file")
			return
		}
		const fileContent = await RealSystem.kernel.read(file)
		props.terminal.writeln(fileContent)
	},
	cd: async (props: rshProps, args: string[], { cd }) => {
		if (!args.length) {
			props.terminal.writeln("cd: you must specify a directory")
			return
		}
		let dir = args[0]
		if (dir == "~") dir = "/home"
		if (!dir.startsWith("/")) {
			dir = RealSystem.kernel.join(props.cwd, dir)
		}
		if (!(await RealSystem.kernel.exists(dir))) {
			props.terminal.writeln("ls: directory does not exist")
			return
		}
		if (!(await RealSystem.kernel.isdir(dir))) {
			props.terminal.writeln("ls: path is not a directory")
			return
		}
		cd(dir)
	},
}

const cwdToShortPath = (cwd: string) => {
	if (cwd == "/") return "/"
	if (cwd == `/home/${RealSystem.kernel.getUser()}`) return "~"
	if (cwd.endsWith("/")) cwd = cwd.slice(0, -1)
	return cwd.split("/").pop()
}

export const rsh = ({ readline, ...state }: rshProps) => {
	readline.read(`[user@realsystem ${cwdToShortPath(state.cwd)}]$ `).then(async (line) => {
		const appCommands = RealSystem.kernel.getApps().map((app) => app.executable)
		const args = split(line)
		const command = args.shift()
		console.log(command, args, appCommands)
		if (command) {
			if (command in builtinCommands) {
				await builtinCommands[command]({ readline, ...state }, args, { cd: (path) => (state.cwd = path) })
			} else if (appCommands.includes(command)) {
				RealSystem.kernel.start(command)
			}
		}
		rsh({ readline, ...state })
	})
}

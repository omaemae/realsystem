import styles from "@/apps/settings/pages/Home.module.css"
import { Appearance } from "@/apps/settings/pages/Appearance"
import { System } from "@/apps/settings/pages/System"
import { PageProps } from "@/apps/settings/settings.app"
import { ComputerDesktopIcon, PaintBrushIcon } from "@heroicons/react/24/solid"
import { ComponentChildren } from "preact"

type HomeProps = {
	switchPage: (page: ComponentChildren) => void
} & PageProps

export const Home = (props: HomeProps) => (
	<div className={styles.home}>
		<h1>RealSystem Settings</h1>
		<div className={styles.pages}>
			<button onClick={() => props.switchPage(<Appearance back={props.back} />)}>
				<PaintBrushIcon />
				<p>Appearance</p>
			</button>
			<button onClick={() => props.switchPage(<System back={props.back} />)}>
				<ComputerDesktopIcon />
				<p>System</p>
			</button>
		</div>
	</div>
)

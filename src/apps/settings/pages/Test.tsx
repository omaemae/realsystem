import { Sidebar, SidebarOption } from "@/apps/settings/components/Sidebar"
import { PageProps } from "@/apps/settings/settings.app"
import { QuestionMarkCircleIcon } from "@heroicons/react/24/solid"
import { useState } from "preact/hooks"

const testOptions: SidebarOption[] = [
	{
		name: "Test",
		icon: <QuestionMarkCircleIcon />,
		page: <p>test</p>,
	},
]

export const Test = (props: PageProps) => {
	const [currentOption, setCurrentOption] = useState(testOptions[0])
	return (
		<div className="flex h-full">
			<Sidebar
				title="Test"
				options={testOptions}
				currentOption={currentOption}
				onBack={() => props.back()}
				onSwitch={(option) => setCurrentOption(option)}
			/>
			<div>{currentOption.page}</div>
		</div>
	)
}

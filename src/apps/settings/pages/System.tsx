import { Sidebar, SidebarOption } from "@/apps/settings/components/Sidebar"
import { About } from "@/apps/settings/pages/system/About"
import { Storage } from "@/apps/settings/pages/system/Storage"
import { PageProps } from "@/apps/settings/settings.app"
import { InformationCircleIcon, ServerStackIcon } from "@heroicons/react/24/solid"
import { useState } from "preact/hooks"

const subPages: SidebarOption[] = [
	{
		name: "About",
		icon: <InformationCircleIcon />,
		page: <About />,
	},
	{
		name: "Storage",
		icon: <ServerStackIcon />,
		page: <Storage />,
	},
]

export const System = (props: PageProps) => {
	const [currentOption, setCurrentOption] = useState(subPages[0])
	return (
		<div className="flex h-full">
			<Sidebar
				title="System"
				options={subPages}
				currentOption={currentOption}
				onBack={() => props.back()}
				onSwitch={(option) => setCurrentOption(option)}
			/>
			{currentOption.page}
		</div>
	)
}

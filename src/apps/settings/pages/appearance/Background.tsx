import { CheckCircleIcon } from "@heroicons/react/24/solid"
import { useEffect, useState } from "preact/hooks"

const readFileAsBlob = async (path: string) => {
	const buffer = await RealSystem.kernel.read(path)
	const blob = new Blob([buffer])
	return URL.createObjectURL(blob)
}

const BackgroundImage = (props: { blob: string; active: boolean; onClick: () => void }) => (
	<button
		className="flex aspect-video w-48 items-center justify-center rounded-md border border-slate-400 bg-cover bg-center shadow"
		style={{ backgroundImage: `url(${props.blob})` }}
		onClick={() => props.onClick()}
	>
		{props.active && (
			<div className="rounded-full bg-blue-700 p-2">
				<CheckCircleIcon className="h-8 w-8 text-white" />
			</div>
		)}
	</button>
)

export const Background = () => {
	const [activeBG, setActiveBG] = RealSystem.user.useRegistryState(`System/UserData/${RealSystem.kernel.getUser()}/Settings/Background`)
	const [backgrounds, setBackgrounds] = useState<string[]>([])
	const [backgroundBlobs, setBackgroundBlobs] = useState<string[]>([])

	useEffect(() => {
		const backgroundPath = "/usr/share/backgrounds"
		;(async () => {
			const elems = (await RealSystem.kernel.readdir(backgroundPath)).map((elem) => RealSystem.kernel.join(backgroundPath, elem))
			const isFile = await Promise.all(elems.map((elem) => RealSystem.kernel.isfile(elem)))
			const files = elems.filter((_, i) => isFile[i])
			setBackgrounds(files)
		})()
	}, [])

	useEffect(() => {
		;(async () => {
			const blobs = await Promise.all(backgrounds.map((bg) => readFileAsBlob(bg)))
			setBackgroundBlobs(blobs)
		})()
	}, [backgrounds])

	return (
		<div className="flex flex-wrap items-start gap-4 self-start p-4">
			{backgroundBlobs.map((blob, i) => (
				<BackgroundImage blob={blob} active={backgrounds[i] == activeBG} onClick={() => setActiveBG(backgrounds[i])} />
			))}
		</div>
	)
}

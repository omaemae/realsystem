export const About = () => (
	<div className="flex flex-col p-4">
		<h1 className="pb-2 text-xl font-semibold">About RealSystem</h1>
		<p>
			<span className="font-semibold">Desktop Experience version:</span> {RealSystem.kernel.getVersion().shell}
		</p>
		<p>
			<span className="font-semibold">Kernel version:</span> {RealSystem.kernel.getVersion().kernel}
		</p>
	</div>
)

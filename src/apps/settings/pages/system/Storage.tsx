import { ExclamationTriangleIcon, XMarkIcon } from "@heroicons/react/24/solid"
import { useState } from "preact/hooks"

export const Storage = () => {
	const [confirm, setConfirm] = useState(false)

	return (
		<div className="relative flex w-full flex-col items-start gap-4 p-4">
			<p>TODO: show storage</p>
			<RealSystem.ui.Button onClick={() => setConfirm(true)}>
				<ExclamationTriangleIcon />
				Wipe all data
			</RealSystem.ui.Button>
			{confirm && (
				<div className="absolute top-0 left-0 flex h-full w-full flex-col items-center justify-center gap-4 bg-black/70">
					<h2 className="text-xl font-bold text-white">Are you sure you want to do this?</h2>
					<div className="flex gap-4">
						<RealSystem.ui.Button onClick={() => RealSystem.kernel._wipeData().then(() => location.reload())}>
							<ExclamationTriangleIcon />
							Wipe all data
						</RealSystem.ui.Button>
						<RealSystem.ui.Button onClick={() => setConfirm(false)}>
							<XMarkIcon />
							Cancel
						</RealSystem.ui.Button>
					</div>
				</div>
			)}
		</div>
	)
}

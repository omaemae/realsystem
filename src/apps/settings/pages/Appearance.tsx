import { Sidebar, SidebarOption } from "@/apps/settings/components/Sidebar"
import { Background } from "@/apps/settings/pages/appearance/Background"
import { PageProps } from "@/apps/settings/settings.app"
import { PhotoIcon } from "@heroicons/react/24/solid"
import { useState } from "preact/hooks"

const subPages: SidebarOption[] = [
	{
		name: "Background",
		icon: <PhotoIcon />,
		page: <Background />,
	},
]

export const Appearance = (props: PageProps) => {
	const [currentOption, setCurrentOption] = useState(subPages[0])
	return (
		<div className="flex h-full">
			<Sidebar
				title="Appearance"
				options={subPages}
				currentOption={currentOption}
				onBack={() => props.back()}
				onSwitch={(option) => setCurrentOption(option)}
			/>
			{currentOption.page}
		</div>
	)
}

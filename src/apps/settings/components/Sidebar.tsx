import { ComponentChild, ComponentChildren } from "preact"
import styles from "@/apps/settings/components/Sidebar.module.css"
import { ArrowLeftIcon } from "@heroicons/react/24/solid"
import classNames from "classnames"

export type SidebarOption = {
	name: string
	page: ComponentChildren
	icon: ComponentChild
}

type SidebarProps = {
	title: string
	options: SidebarOption[]
	currentOption: SidebarOption
	onBack: () => void
	onSwitch: (option: SidebarOption) => void
}

export const Sidebar = (props: SidebarProps) => (
	<div className={styles.sidebar}>
		<button className={styles.titleButton} onClick={() => props.onBack()}>
			<ArrowLeftIcon />
			{props.title}
		</button>
		{props.options.map((option) => (
			<button className={classNames(props.currentOption == option && styles.active)} onClick={() => props.onSwitch(option)}>
				{option.icon}
				{option.name}
			</button>
		))}
	</div>
)

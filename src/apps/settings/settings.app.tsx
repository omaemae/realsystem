import { Home } from "@/apps/settings/pages/Home"
import { Application } from "@/kernel/app"
import { ComponentChildren } from "preact"
import { useState } from "preact/hooks"

export type PageProps = {
	back: () => void
}

const SettingsWindow = () => {
	const homePage = <Home switchPage={(page) => setCurrentPage(page)} back={() => setCurrentPage(homePage)} />
	const [currentPage, setCurrentPage] = useState<ComponentChildren>(homePage)
	return <>{currentPage}</>
}

class SettingsApp implements Application {
	name = "Settings"
	executable = "settings"
	author = "omame"

	main() {
		RealSystem.shell.openWindow({
			title: "Settings",
			children: <SettingsWindow />,
			width: 700,
			height: 500,
		})
	}
}

export default SettingsApp

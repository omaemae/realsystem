import { Application } from "@/kernel/app"
import styles from "@/apps/rsver/styles.module.css"

const AboutWindow = () => (
	<div className={styles.window}>
		<div className={styles.band}>
			<h1>RealSystem</h1>
		</div>
		<div className={styles.body}>
			<p>
				<b>RealSystem</b>
			</p>
			<p>
				Version {RealSystem.kernel.getVersion().shell} (OS Build {RealSystem.kernel.getVersion().kernel})
			</p>
			<p>Copyright &copy; omame 2022</p>
		</div>
	</div>
)

class AboutApp implements Application {
	name = "About RealSystem"
	executable = "rsver"
	author = "omame"

	main() {
		RealSystem.shell.openWindow({
			title: this.name,
			children: <AboutWindow />,
		})
	}
}

export default AboutApp

// noinspection JSPotentiallyInvalidConstructorUsage

import { useEffect, useRef, useState } from "preact/hooks"
import { Application } from "@/kernel/app"
import { JSX } from "preact/jsx-runtime"
import { ShellMain } from "@/shell"
import styles from "@/kernel/index.module.css"
import fsSyscalls, { mountPoints } from "@/kernel/fs/syscalls"
import { encode } from "base64-arraybuffer"
import { version } from "@/globals"
import { registryInit } from "@/user/registry"
import { connectToRSNet, getIP, sendPacket, sendPing } from "@/kernel/net"

const realLogHistory: [Date, string][] = []
const realApps: Application[] = []

export type KernelExports = {
	log: (line: string) => void
	loadApp: (app: Application) => void
	start: (executable: string, args?: string[]) => void
	getApps: () => Application[]
	getVersion: () => { kernel: string; shell: string }
	createUser: (username: string) => Promise<void>
	userLogin: (username: string) => void
	getUser: () => string | undefined
	open: (path: string) => void

	_wipeData: () => Promise<void>

	// FS syscalls
	getMountPoints: () => string[]
	mount: (mountPoint: string) => Promise<void>
	exists: (path: string) => Promise<boolean>
	mkfile: (path: string) => Promise<void>
	mkdir: (path: string) => Promise<void>
	delete: (path: string) => Promise<void>
	read: (path: string) => Promise<Uint8Array>
	write: (path: string, content: Uint8Array) => Promise<void>
	readdir: (path: string) => Promise<string[]>
	isfile: (path: string) => Promise<boolean>
	isdir: (path: string) => Promise<boolean>
	join: (pathA: string, pathB: string) => string

	// network syscalls
	getIP: () => string | undefined
	sendPing: (dest: string, data: string) => void
	sendPacket: (dest: string, port: number, data: string) => void
}

const wipeData = async () => {
	RealSystem.kernel.log("/!\\ WIPING DATA /!\\")
	RealSystem.kernel.log("Removing registry data...")
	delete localStorage["registry"]
	RealSystem.kernel.log("Unmounting and removing partitions...")
	const mounts = Object.values(mountPoints)
	for (const mount of mounts) {
		await mount.unload()
		await mount._wipeData()
	}
	RealSystem.kernel.log("Done. RealSystem will be unstable from this point forward, as the root partition and registry are gone.")
}

const createUser = async (username: string) => {
	if (/\//.test(username)) {
		throw new Error("Invalid username.")
	}

	// create /home if it doesnt exist
	if (!(await RealSystem.kernel.exists("/home"))) {
		await RealSystem.kernel.mkdir("/home")
	}

	// create /home/${user}
	await RealSystem.kernel.mkdir(`/home/${username}`)

	// check if user is in System/Users
	const users = RealSystem.user.registryRead<string[]>("System/Users")
	if (users?.includes(username)) {
		// no
		throw new Error(`User ${username} already exists.`)
	}

	// add user to System/Users
	users.push(username)
	RealSystem.user.registryWrite("System/Users", users)

	// add user data to System/UserData
	RealSystem.user.registryWrite(`System/UserData/${username}`, {
		Settings: {
			Background: "/usr/share/backgrounds/realsystem.png",
		},
	})
}

export const stubKernelExports: KernelExports = {
	log: () => {},
	loadApp: () => {},
	start: () => {},
	getApps: () => realApps,
	getVersion: () => version,
	createUser,
	userLogin: () => {},
	getUser: () => undefined,
	open: () => {},
	_wipeData: wipeData,

	...fsSyscalls,

	getIP,
	sendPing,
	sendPacket,
}

export class LoginEvent extends Event {
	username: string

	constructor(username: string) {
		super("realsystem.login")
		this.username = username
	}
}

export const KernelMain = () => {
	const [logHistory, setLogHistory] = useState<[Date, string][]>([])
	const kernelLogDiv = useRef<HTMLDivElement>(null)
	const [_, setApps] = useState<Application[]>([])
	const [shell, setShell] = useState<JSX.Element>()
	const [currentUser, setCurrentUser] = useState<string>()

	const log = (line: string) => {
		realLogHistory.push([new Date(), line])
		setLogHistory([...realLogHistory])
		console.log(line)
	}

	const loadApp = (app: Application) => {
		realApps.push(app)
		setApps([...realApps])
		log("Loaded app: " + (app.name || app.executable) + " by " + app.author)
	}

	const start = (executable: string, args?: string[]) => {
		log("Starting app " + executable)
		const filteredApps = realApps.filter((app) => app.executable === executable)
		if (filteredApps.length !== 1) {
			log("App " + executable + " does not exist")
			throw new Error(`App ${executable} does not exist.`)
		}
		const app = filteredApps[0]
		try {
			app.main(args)
		} catch (error) {
			log(`An uncaught exception has occured in ${app.executable}: ${error}\nSee JS console for detailed stack trace.`)
			console.error(error)
		}
	}

	const userLogin = (username: string) => {
		if (currentUser) throw new Error("You cannot start a new session while a session is already active.")
		const users = RealSystem.user.registryRead<string[]>("System/Users")
		if (!users.includes(username)) throw new Error("This user does not exist.")
		log(`Starting a new session as ${username}...`)
		setCurrentUser(username)
		window.dispatchEvent(new LoginEvent(username))
	}

	const open = async (path: string) => {
		log(`Opening file ${path}...`)
		// get extension
		const regexResult = /.+\.(\w+)/.exec(path) as [string, string] | null
		if (!regexResult) {
			// no extension
			if (await RealSystem.kernel.isdir(path)) {
				// if it's a folder, open an explorer window
				RealSystem.kernel.start("explorer", [path])
			} else {
				// otherwise, show "choose an app" dialog
				RealSystem.shell.openWithApp(path)
			}
			return
		}
		const [cleanPath, extension] = regexResult
		// get default app to open this extension
		const defaultExecutable = RealSystem.user.registryRead<string | undefined>(`System/FileAssociations/Extensions/${extension}`)
		if (defaultExecutable) {
			// if we have a default executable, start the app
			RealSystem.kernel.start(defaultExecutable, [cleanPath])
		} else {
			// otherwise, show "choose an app" dialog
			RealSystem.shell.openWithApp(cleanPath, extension)
		}
	}

	RealSystem.kernel = {
		log,
		loadApp,
		start,
		getApps: () => realApps,
		getVersion: () => version,
		createUser,
		userLogin,
		getUser: () => currentUser,
		open,
		_wipeData: wipeData,

		...fsSyscalls,

		getIP,
		sendPing,
		sendPacket,
	}

	useEffect(() => {
		if (!kernelLogDiv.current) return
		kernelLogDiv.current.scrollTop = kernelLogDiv.current.scrollHeight
	}, [logHistory])

	useEffect(() => {
		;(async () => {
			// kernel init
			registryInit()
			await RealSystem.kernel.mount("/")
			connectToRSNet()

			// download rootfs
			log("Downloading rootfs to /...")
			const rootfsData = import.meta.glob("@rootfs/**/*", { as: "url" })
			const rootfsFiles = Object.keys(rootfsData).map((file) => file.replace("/rootfs", ""))
			const rootfsFileURLs = await Promise.all(Object.keys(rootfsData).map((file) => rootfsData[file]()))

			let forceUpdate
			if (crypto.subtle) {
				// if we have subtle crypto, force rootfs update if out of date
				const filesHash = encode(await crypto.subtle.digest("SHA-512", new TextEncoder().encode(rootfsFiles.join(" "))))
				const oldHash = localStorage.getItem("filesHash")
				forceUpdate = oldHash == null || filesHash != oldHash
				localStorage.setItem("filesHash", filesHash)
			} else {
				// if we dont have subtle crypto, always download rootfs
				forceUpdate = true
			}

			for (const file of rootfsFiles) {
				if ((await RealSystem.kernel.exists(file)) && !forceUpdate) continue
				log("Downloading " + file + "...")
				const pathSegments = file
					.split("/")
					.slice(1, -1)
					.map(
						(_, i) =>
							"/" +
							file
								.split("/")
								.slice(1, i + 2)
								.join("/")
					)
				for (const segment of pathSegments) {
					if (await RealSystem.kernel.exists(segment)) continue
					await RealSystem.kernel.mkdir(segment)
				}
				if (!(await RealSystem.kernel.exists(file))) await RealSystem.kernel.mkfile(file)
				const fileURL = rootfsFileURLs[rootfsFiles.indexOf(file)]
				const resp = await fetch(fileURL)
				const buffer = await resp.arrayBuffer()
				await RealSystem.kernel.write(file, new Uint8Array(buffer))
			}

			// load built-in apps
			log("Loading built-in apps...")
			const appModules = import.meta.glob("@/apps/**/*.app.tsx")
			const modules = await Promise.all(Object.values(appModules).map((load) => load()))
			const apps: Application[] = modules
				.map((module, i) => {
					try {
						return new (module as Record<string, any>).default()
					} catch {
						log(`Failed to load app from ${Object.keys(appModules)[i]}`)
					}
				})
				.filter((app) => app)
			apps.forEach((app) => loadApp(app))

			// load apps from /usr/apps
			log("Loading installed apps...")
			const usrApps = await RealSystem.kernel.readdir("/usr/apps")
			for (const app of usrApps) {
				try {
					if (await RealSystem.kernel.isdir(`/usr/apps/${app}`)) continue
					const data = await RealSystem.kernel.read(`/usr/apps/${app}`)
					const blob = new Blob([data], { type: "application/javascript" })
					const module = await import(/* @vite-ignore */ URL.createObjectURL(blob))
					loadApp(new (module as Record<string, any>).default())
				} catch (error) {
					log(`Failed to load installed app ${app}. See JS console for more information.`)
					console.error(error)
				}
			}

			// open shell
			log("Starting shell...")
			setShell(<ShellMain />)
		})()
	}, [])

	return (
		<>
			<div className={styles.kernelLog} ref={kernelLogDiv}>
				{logHistory.map(([date, line]) => (
					<p>
						<span className={styles.time}>
							[{date.getHours().toString().padStart(2, "0")}:{date.getMinutes().toString().padStart(2, "0")}:
							{date.getSeconds().toString().padStart(2, "0")}]
						</span>{" "}
						{line}
					</p>
				))}
			</div>
			{shell}
		</>
	)
}

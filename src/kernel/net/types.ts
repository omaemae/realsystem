export const protocolVersion = 1

export enum RSNetEvent {
	WELCOME,
	KEEPALIVE,

	// Communication protocol
	REGISTER,
	PING,
	PACKET,
	NETWORK_ERROR,
}

export enum RSNetworkError {
	DEST_NOT_FOUND,
}

export enum WSCloseCode {
	INVALID_DATA = 4000,
	NO_KEEPALIVE,
	REGISTER_REQUIRED,
	ALREADY_EXISTS,
}

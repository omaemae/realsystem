import { handleRegister, handleWelcome, RSNetClient } from "@/kernel/net/protocol"
import { RSNetEvent } from "@/kernel/net/types"

const rsnet = "wss://net.rs.omame.xyz/ws"

export const generateMAC = () =>
	new Array(6)
		.fill(0)
		.map(() =>
			Math.round(Math.random() * 255)
				.toString(16)
				.toUpperCase()
				.padStart(2, "0")
		)
		.join(":")

let ws: WebSocket
let client: RSNetClient | undefined

class RSNetPingEvent extends Event {
	source: string
	data: string

	constructor(source: string, data: string) {
		super("realsystem.network.ping")
		this.source = source
		this.data = data
	}
}

class RSNetPacketEvent extends Event {
	source: string
	port: number
	data: string

	constructor(source: string, port: number, data: string) {
		super("realsystem.network.packet")
		this.source = source
		this.port = port
		this.data = data
	}
}

export const connectToRSNet = () => {
	if (ws?.readyState === WebSocket.OPEN) return
	RealSystem.kernel.log("RSNet: Connecting to RSNet...")
	ws = new WebSocket(rsnet)
	ws.onopen = () => RealSystem.kernel.log("RSNet: Connected to RSNet server.")
	ws.onmessage = (e) => {
		const data: { event: RSNetEvent; [x: string]: unknown } = JSON.parse(e.data)
		switch (data.event) {
			case RSNetEvent.WELCOME:
				client = handleWelcome(ws, data.version as number)
				break
			case RSNetEvent.REGISTER:
				handleRegister(client!, data.ip as string)
				break
			case RSNetEvent.PING:
				window.dispatchEvent(new RSNetPingEvent(data.source as string, data.data as string))
				break
			case RSNetEvent.PACKET:
				window.dispatchEvent(new RSNetPacketEvent(data.source as string, data.port as number, data.data as string))
				break
		}
	}
	ws.onclose = () => {
		RealSystem.kernel.log("RSNet: Disconnected from RSNet, reconnecting in 5 seconds...")
		client = undefined
		setTimeout(() => connectToRSNet(), 5000)
	}
}

export const getIP = () => client && client.ip
export const sendPing = (dest: string, data: string) => client && client.send(RSNetEvent.PING, { dest, data })
export const sendPacket = (dest: string, port: number, data: string) => client && client.send(RSNetEvent.PACKET, { dest, port, data })

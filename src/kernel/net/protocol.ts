import { generateMAC } from "@/kernel/net"
import { protocolVersion, RSNetEvent } from "@/kernel/net/types"

export class RSNetClient {
	socket: WebSocket
	mac?: string
	ip?: string
	pingInterval?: number

	constructor(socket: WebSocket) {
		this.socket = socket
	}

	send(event: RSNetEvent, data: Record<string, unknown> = {}) {
		this.socket.send(
			JSON.stringify({
				event,
				...data,
			})
		)
	}
}

export const handleWelcome = (socket: WebSocket, version: number) => {
	if (version != protocolVersion) {
		RealSystem.kernel.log(`RSNet: Client or server are out of date, disconnecting.`)
		socket.close()
		return undefined
	}
	const client = new RSNetClient(socket)
	let mac = RealSystem.user.registryRead<string | null>("Network/MAC")
	if (!mac) {
		RealSystem.kernel.log(`RSNet: Generating new MAC...`)
		mac = generateMAC()
		RealSystem.user.registryWrite("Network/MAC", mac)
		RealSystem.kernel.log(`RSNet: New MAC: ${mac}`)
	}
	client.mac = mac
	client.send(RSNetEvent.REGISTER, { mac })
	return client
}

export const handleRegister = (client: RSNetClient, ip: string) => {
	client.ip = ip
	RealSystem.kernel.log(`RSNet: Was assigned IP ${ip}.`)
	client.pingInterval = setInterval(() => client.send(RSNetEvent.KEEPALIVE), 30 * 1000) as unknown as number
}

export const handleClose = (client: RSNetClient) => {
	clearInterval(client.pingInterval)
}

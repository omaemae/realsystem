export enum FileType {
	FILE,
	DIRECTORY,
	SYMLINK,
}

interface _FileBase {
	path: string
}

interface _File extends _FileBase {
	type: FileType.FILE
	content: Uint8Array
}

interface _Symlink extends _FileBase {
	type: FileType.SYMLINK
	content: string
}

interface _Directory extends _FileBase {
	type: FileType.DIRECTORY
	content: string[]
}

export type File = _File | _Symlink | _Directory

export interface FileSystemDriver {
	name: string
	mounted: boolean

	load(mountPoint: string): Promise<void>
	unload(): Promise<void>
	_wipeData(): Promise<void>

	getFile(path: string): Promise<File>
	exists(path: string): Promise<boolean>
	putFile(file: File): Promise<void>
	deleteFile(path: string): Promise<void>
}

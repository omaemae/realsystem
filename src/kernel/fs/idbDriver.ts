import { File, FileSystemDriver } from "@/kernel/fs/driver"

// IDB driver data structure
// Directories - files with content: string[]
// Files - files with content: Uint8Array
// Root directory - /

export class IDBDriver implements FileSystemDriver {
	name = "idb"
	mounted = false
	mountPoint: string | undefined

	private db: IDBDatabase | undefined

	load(mountPoint: string) {
		this.mountPoint = mountPoint
		return new Promise<void>((resolve, reject) => {
			const request = window.indexedDB.open("RealSystem IDB FS at " + mountPoint, 3)
			request.onerror = () => {
				RealSystem.kernel.log(`IDB FS: Failed to open IDB: ${request.error}`)
				reject(request.error)
			}
			request.onsuccess = () => {
				this.db = request.result
				this.db.onerror = (event) => {
					RealSystem.kernel.log(`IDB FS: Database error: ${(event.target as Record<string, unknown> | null)?.error}`)
				}
				this.mounted = true
				RealSystem.kernel.log(`IDB FS: Mounted ${mountPoint}.`)
				resolve()
			}
			request.onupgradeneeded = (event) => {
				const db = (event.target as Record<string, unknown> | null)?.result as IDBDatabase

				db.createObjectStore("files", { keyPath: "path" })
			}
		})
	}

	async unload() {
		this.db?.close()
		RealSystem.kernel.log(`IDB FS: Unmounted ${this.mountPoint}.`)
	}

	_wipeData(): Promise<void> {
		return new Promise<void>((resolve) => {
			const request = window.indexedDB.deleteDatabase("RealSystem IDB FS at " + this.mountPoint)
			request.onsuccess = () => {
				RealSystem.kernel.log(`IDB FS: Deleted ${this.mountPoint}.`)
				resolve()
			}
		})
	}

	getFile(path: string): Promise<File> {
		if (!this.db) throw new Error("Database is not initialized.")
		const transaction = this.db?.transaction(["files"], "readonly")
		return new Promise<File>((resolve, reject) => {
			const objectStore = transaction.objectStore("files")
			const request = objectStore.get(path)
			request.onerror = () => {
				RealSystem.kernel.log(`IDB FS: Failed getting file ${path}: ${request.error}.`)
				reject(request.error)
			}
			request.onsuccess = () => {
				resolve(request.result as File)
			}
		})
	}

	exists(path: string): Promise<boolean> {
		if (!this.db) throw new Error("Database is not initialized.")
		const transaction = this.db?.transaction(["files"], "readonly")
		return new Promise<boolean>((resolve, reject) => {
			const objectStore = transaction.objectStore("files")
			const request = objectStore.count(path)
			request.onerror = () => {
				RealSystem.kernel.log(`IDB FS: Exists for ${path} failed: ${request.error}.`)
				reject(request.error)
			}
			request.onsuccess = () => {
				resolve(request.result === 1)
			}
		})
	}

	putFile(file: File): Promise<void> {
		if (!this.db) throw new Error("Database is not initialized.")
		const transaction = this.db?.transaction(["files"], "readwrite")
		return new Promise<void>((resolve, reject) => {
			const objectStore = transaction.objectStore("files")
			const request = objectStore.put(file)
			request.onerror = () => {
				RealSystem.kernel.log(`IDB FS: Putting file at path ${file.path} failed: ${request.error}.`)
				reject(request.error)
			}
			transaction.oncomplete = () => {
				resolve()
			}
		})
	}

	deleteFile(path: string): Promise<void> {
		if (!this.db) throw new Error("Database is not initialized.")
		const transaction = this.db?.transaction(["files"], "readwrite")
		return new Promise<void>((resolve, reject) => {
			const objectStore = transaction.objectStore("files")
			const request = objectStore.delete(path)
			request.onerror = () => {
				RealSystem.kernel.log(`IDB FS: Deleting file at path ${path} failed: ${request.error}.`)
				reject(request.error)
			}
			transaction.oncomplete = () => {
				resolve()
			}
		})
	}
}

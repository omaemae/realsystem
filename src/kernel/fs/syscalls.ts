import { FileSystemDriver, File, FileType } from "@/kernel/fs/driver"
import { IDBDriver } from "@/kernel/fs/idbDriver"

export const mountPoints: Record<string, FileSystemDriver> = {}

const getMountPointFromPath = (path: string) => {
	const longestMountPoints = Object.keys(mountPoints).sort((a, b) => b.length - a.length)
	for (const mountPoint of longestMountPoints) {
		if (path.startsWith(mountPoint)) {
			return mountPoint
		}
	}
}

const getMountPoints = () => Object.keys(mountPoints)

const mount = async (mountPoint: string) => {
	for (const Driver of [IDBDriver]) {
		try {
			const driver = new Driver()
			RealSystem.kernel.log(`Mounting ${mountPoint} with ${driver.name} driver`)
			await driver.load(mountPoint)
			mountPoints[mountPoint] = driver
			if (!(await driver.exists("/"))) {
				await driver.putFile({ path: "/", type: FileType.DIRECTORY, content: [] })
			}
			return
		} catch {
			RealSystem.kernel.log("Mount failed, trying with next driver...")
		}
	}
	RealSystem.kernel.log("Mount failed, ran out of drivers to try.")
	throw new Error("All drivers failed to mount filesystem.")
}

const getFSPath = (path: string, mountPoint: string) => "/" + path.slice(mountPoint.length)

const join = (pathA: string, pathB: string) => {
	const pathSegmentsA = pathA.split("/")
	const pathSegmentsB = pathB.split("/")

	for (const segment of pathSegmentsB) {
		if (segment == "..") {
			pathSegmentsA.pop()
			continue
		}
		if (segment == ".") {
			continue
		}
		pathSegmentsA.push(segment)
	}

	const newPath = pathSegmentsA.join("/")
	return newPath.replace("//", "/") || "/"
}

const exists = async (path: string) => {
	const mountPoint = getMountPointFromPath(path)
	if (!mountPoint) throw new Error("Invalid path.")
	const driver = mountPoints[mountPoint]
	const fsPath = getFSPath(path, mountPoint)
	const pathSegments = fsPath
		.split("/")
		.slice(1)
		.map(
			(_, i) =>
				"/" +
				fsPath
					.split("/")
					.slice(1, i + 2)
					.join("/")
		)
	const pathSegmentsExist = await Promise.all(pathSegments.map((path) => driver.exists(path)))
	return !pathSegmentsExist.includes(false)
}

const mkfile = async (path: string) => {
	const mountPoint = getMountPointFromPath(path)
	if (!mountPoint) throw new Error("Invalid path.")
	const driver = mountPoints[mountPoint]
	const fsPath = getFSPath(path, mountPoint)
	if (await exists(fsPath)) throw new Error("File exists.")
	const backPath = join(path, "..")
	const backFsPath = getFSPath(backPath, getMountPointFromPath(backPath)!)
	if (!(await exists(backPath))) throw new Error("Parent directory does not exist.")
	const parentDirectory = await driver.getFile(backFsPath)
	if (parentDirectory.type != FileType.DIRECTORY) throw new Error("Parent directory does not exist.")
	const file: File = {
		path: fsPath,
		type: FileType.FILE,
		content: new Uint8Array(0),
	}
	parentDirectory.content.push(fsPath)
	await driver.putFile(file)
	await driver.putFile(parentDirectory)
}

const mkdir = async (path: string) => {
	const mountPoint = getMountPointFromPath(path)
	if (!mountPoint) throw new Error("Invalid path.")
	const driver = mountPoints[mountPoint]
	const fsPath = getFSPath(path, mountPoint)
	if (await exists(fsPath)) throw new Error("File exists.")
	const backPath = join(path, "..")
	const backFsPath = getFSPath(backPath, getMountPointFromPath(backPath)!)
	if (!(await exists(backPath))) throw new Error("Parent directory does not exist.")
	const parentDirectory = await driver.getFile(backFsPath)
	if (parentDirectory.type != FileType.DIRECTORY) throw new Error("Parent directory does not exist.")
	const file: File = {
		path: fsPath,
		type: FileType.DIRECTORY,
		content: [],
	}
	parentDirectory.content.push(fsPath)
	await driver.putFile(file)
	await driver.putFile(parentDirectory)
}

const _delete = async (path: string) => {
	const mountPoint = getMountPointFromPath(path)
	if (!mountPoint) throw new Error("Invalid path.")
	const driver = mountPoints[mountPoint]
	const fsPath = getFSPath(path, mountPoint)
	const backPath = join(path, "..")
	const backFsPath = getFSPath(backPath, getMountPointFromPath(backPath)!)
	if (!(await exists(backPath))) throw new Error("Parent directory does not exist.")
	const parentDirectory = await driver.getFile(backFsPath)
	if (parentDirectory.type != FileType.DIRECTORY) throw new Error("Parent directory does not exist.")
	const file = await driver.getFile(fsPath)
	if (file.type == FileType.DIRECTORY && file.content.length != 0) throw new Error("Directory is not empty.")
	parentDirectory.content.splice(parentDirectory.content.indexOf(fsPath))
	await driver.deleteFile(fsPath)
	await driver.putFile(parentDirectory)
}

const read = async (path: string) => {
	const mountPoint = getMountPointFromPath(path)
	if (!mountPoint) throw new Error("Invalid path.")
	const driver = mountPoints[mountPoint]
	const fsPath = getFSPath(path, mountPoint)
	const backPath = join(path, "..")
	if (!(await exists(backPath))) throw new Error("Parent directory does not exist.")
	const file = await driver.getFile(fsPath)
	if (file.type != FileType.FILE) throw new Error("Path is not a file.")
	return file.content
}

const write = async (path: string, content: Uint8Array) => {
	const mountPoint = getMountPointFromPath(path)
	if (!mountPoint) throw new Error("Invalid path.")
	const driver = mountPoints[mountPoint]
	const fsPath = getFSPath(path, mountPoint)
	const backPath = join(path, "..")
	if (!(await exists(backPath))) throw new Error("Parent directory does not exist.")
	const file = await driver.getFile(fsPath)
	if (file.type != FileType.FILE) throw new Error("Path is not a file.")
	file.content = content
	await driver.putFile(file)
}

const readdir = async (path: string) => {
	const mountPoint = getMountPointFromPath(path)
	if (!mountPoint) throw new Error("Invalid path.")
	const driver = mountPoints[mountPoint]
	const fsPath = getFSPath(path, mountPoint)
	const backPath = join(path, "..")
	if (!(await exists(backPath))) throw new Error("Parent directory does not exist.")
	const file = await driver.getFile(fsPath)
	if (file.type != FileType.DIRECTORY) throw new Error("Path is not a directory.")
	return file.content.map((path) => path.slice(fsPath.length + (fsPath == "/" ? 0 : 1)))
}

const isfile = async (path: string) => {
	const mountPoint = getMountPointFromPath(path)
	if (!mountPoint) throw new Error("Invalid path.")
	const driver = mountPoints[mountPoint]
	const fsPath = getFSPath(path, mountPoint)
	const backPath = join(path, "..")
	if (!(await exists(backPath))) throw new Error("Parent directory does not exist.")
	const file = await driver.getFile(fsPath)
	return file.type == FileType.FILE
}

const isdir = async (path: string) => {
	const mountPoint = getMountPointFromPath(path)
	if (!mountPoint) throw new Error("Invalid path.")
	const driver = mountPoints[mountPoint]
	const fsPath = getFSPath(path, mountPoint)
	const backPath = join(path, "..")
	if (!(await exists(backPath))) throw new Error("Parent directory does not exist.")
	const file = await driver.getFile(fsPath)
	return file.type == FileType.DIRECTORY
}

export default {
	getMountPoints,
	mount,
	exists,
	mkfile,
	mkdir,
	delete: _delete,
	read,
	write,
	readdir,
	isfile,
	isdir,
	join,
}

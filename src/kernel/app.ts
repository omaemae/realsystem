export interface Application {
	executable: string
	name?: string
	author?: string
	description?: string
	version?: string
	// List of extensions that this app supports
	extensions?: string[]

	main: (args?: string[]) => any
}

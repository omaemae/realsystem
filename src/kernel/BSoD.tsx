import { pascalCaseToSnakeCase } from "@/internal/helpers"
import { ComponentChildren } from "preact"
import { useErrorBoundary } from "preact/hooks"

export const BSoD = (props: { children: ComponentChildren }) => {
	const [error] = useErrorBoundary()

	const bwueScween = Math.random() < 0.3

	if (error)
		return (
			<div className="flex h-screen w-full items-center justify-center bg-blue-600 text-white">
				<div className="flex w-full max-w-4xl flex-col gap-2 p-12">
					<h1 className="pb-4 text-7xl">{bwueScween ? ":3" : ":("}</h1>
					<p className="font-bold">RealSystem ran into a problem and needs to restart.</p>
					<p>
						Stop code: <span className="font-mono">{pascalCaseToSnakeCase(error.name)}</span>
					</p>

					<pre className="whitespace-pre-wrap pt-4 text-xs">{error.stack}</pre>
				</div>
			</div>
		)

	return <>{props.children}</>
}

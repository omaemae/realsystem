import "@/base.css"
import { render } from "preact"
import { KernelMain, stubKernelExports } from "./kernel"
import { stubShellExports } from "@/shell"
import user from "@/user"
import ui from "@/ui"
import * as preact from "preact"
import * as preactJSX from "preact/jsx-runtime"
import * as preactHooks from "preact/hooks"
import { BSoD } from "@/kernel/BSoD"
import "@fontsource/inter/variable.css"
import "@fontsource/jetbrains-mono"
import "preact/debug"

const exports = {
	kernel: stubKernelExports,
	shell: stubShellExports,
	user,
	ui,
	libraries: {
		preact,
		preactJSX,
		preactHooks,
	},
}

declare global {
	var RealSystem: typeof exports
}

globalThis.RealSystem = exports

render(
	<BSoD>
		<KernelMain />
	</BSoD>,
	document.body
)

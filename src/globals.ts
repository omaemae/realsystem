declare const COMMIT_HASH: string
declare const COMMIT_COUNT: string

export const version = {
	kernel: `r${COMMIT_COUNT}.${COMMIT_HASH}`,
	shell: "1.0.0",
}

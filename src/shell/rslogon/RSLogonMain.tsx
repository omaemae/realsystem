import { readFileAsBlob } from "@/internal/helpers"
import { useState, useEffect } from "preact/hooks"
import styles from "@/shell/rslogon/RSLogonMain.module.css"
import { UserCircleIcon } from "@heroicons/react/24/solid"

export const RSLogonMain = () => {
	const [bgBlob, setBGBlob] = useState("")
	const [users] = RealSystem.user.useRegistryState<string[]>("System/Users")

	useEffect(() => {
		readFileAsBlob("/usr/share/backgrounds/realsystem.png").then((blob) => setBGBlob(blob))
	}, [])

	return (
		<div className={styles.bg} style={{ backgroundImage: `url(${bgBlob})` }}>
			{users.map((user) => (
				<button
					className="flex h-40 w-40 flex-col items-center gap-2 bg-black/40 p-8 text-xl font-semibold text-white backdrop-blur-2xl transition-all hover:scale-105"
					onClick={() => {
						RealSystem.kernel.userLogin(user)
					}}
				>
					<UserCircleIcon className="h-16 w-16" />
					{user}
				</button>
			))}
		</div>
	)
}

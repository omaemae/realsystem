import { useEffect, useRef } from "preact/hooks"
import styles from "@/shell/usershell/StartMenu.module.css"
import { Cog6ToothIcon } from "@heroicons/react/24/solid"

const StartMenu = (props: { onClose: () => void }) => {
	const startDiv = useRef<HTMLDivElement>(null)

	useEffect(() => {
		const listener = (e: MouseEvent) => {
			if (!startDiv.current?.contains(e.target as HTMLElement)) {
				props.onClose()
			}
		}
		window.addEventListener("click", listener)
		return () => {
			window.removeEventListener("click", listener)
		}
	}, [startDiv])

	return (
		<div className={styles.startMenu} ref={startDiv}>
			<div className={styles.left}>
				<button
					onClick={() => {
						RealSystem.kernel.start("settings")
						props.onClose()
					}}
				>
					<Cog6ToothIcon />
				</button>
			</div>
			<div className={styles.apps}>
				{RealSystem.kernel.getApps().map((app) => (
					<button
						onClick={() => {
							RealSystem.kernel.start(app.executable)
							props.onClose()
						}}
					>
						{app.name || app.executable}
					</button>
				))}
			</div>
		</div>
	)
}

export default StartMenu

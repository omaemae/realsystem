import DraggableWindow from "@/shell/usershell/DraggableWindow"
import StartMenu from "@/shell/usershell/StartMenu"
import styles from "@/shell/usershell/index.module.css"
import { useEffect, useState } from "preact/hooks"
import { Squares2X2Icon } from "@heroicons/react/24/solid"
import classNames from "classnames"
import { readFileAsBlob } from "@/internal/helpers"
import { OpenWindow } from "@/shell"
import { OpenWith } from "@/shell/usershell/OpenWith"

const realOpenWindows: OpenWindow[] = []

const Time = () => {
	const [date, setDate] = useState(new Date())

	useEffect(() => {
		setTimeout(() => setDate(new Date()), 2000)
	}, [date])

	return (
		<>
			{date.getHours().toString().padStart(2, "0")}:{date.getMinutes().toString().padStart(2, "0")}
		</>
	)
}

export const WindowManager = (props: { user: string }) => {
	const [openWindows, setOpenWindows] = useState<OpenWindow[]>([])
	const [startOpen, setStartOpen] = useState(false)
	const [focusedWindow, setFocusedWindow] = useState<OpenWindow>()
	const [bg] = RealSystem.user.useRegistryState<string>(`System/UserData/${props.user}/Settings/Background`)
	const [bgBlob, setBGBlob] = useState("")

	const openWindow = (openWindow: OpenWindow) => {
		openWindow.openedAt = new Date()
		RealSystem.kernel.log("Opening window " + openWindow.title)
		realOpenWindows.push(openWindow)
		setOpenWindows([...realOpenWindows])
		setFocusedWindow(openWindow)
	}

	const toggleWindow = (window: OpenWindow) => {
		window.minimized = !window.minimized
		if (!window.minimized) setFocusedWindow(window)
		setOpenWindows([...realOpenWindows]) // force re-render
	}

	const closeWindow = (window: OpenWindow) => {
		realOpenWindows.splice(realOpenWindows.indexOf(window), 1)
		setOpenWindows([...realOpenWindows])
	}

	const openWithApp = (path: string, extension?: string) => {
		openWindow({
			title: `Open with app`,
			children: ({ closeWindow }) => <OpenWith path={path} extension={extension} closeWindow={closeWindow} />,
			height: 400,
		})
	}

	RealSystem.shell = {
		openWindow,
		openWithApp,
	}

	useEffect(() => {
		readFileAsBlob(bg).then((blob) => setBGBlob(blob))
	}, [bg])

	return (
		<div className={styles.shell} style={{ backgroundImage: `url(${bgBlob})` }}>
			<div className={styles.taskbar}>
				<button className={styles.start} onClick={() => setStartOpen(true)}>
					<Squares2X2Icon className="h-6 w-6" />
				</button>
				<div className={styles.tasklist}>
					{openWindows.map((w) => (
						<button
							className={classNames(w.minimized && styles.minimized, focusedWindow?.openedAt == w.openedAt && styles.active)}
							onClick={() => toggleWindow(w)}
						>
							{w.title}
						</button>
					))}
				</div>
				<div className={styles.time}>
					<Time />
				</div>
			</div>
			{startOpen && <StartMenu onClose={() => setStartOpen(false)} />}
			{openWindows.map((w) => (
				<DraggableWindow
					key={w.openedAt}
					title={w.title}
					children={w.children}
					reservedY={40}
					onMinimize={() => toggleWindow(w)}
					onClose={() => closeWindow(w)}
					onFocus={() => setFocusedWindow(w)}
					isFocused={focusedWindow?.openedAt == w.openedAt}
					minimized={w.minimized}
					width={w.width}
					height={w.height}
				/>
			))}
		</div>
	)
}

import { useEffect, useRef, useState } from "preact/hooks"
import { ComponentChildren } from "preact"
import styles from "@/shell/usershell/DraggableWindow.module.css"
import { ArrowsPointingInIcon, ArrowsPointingOutIcon, MinusIcon, XMarkIcon } from "@heroicons/react/24/solid"
import classNames from "classnames"
import { WindowErrorBoundary } from "@/shell/usershell/WindowErrorBoundary"

type DraggableWindowProps = {
	title: string
	children?: DraggableWindowChildren
	onMinimize?: () => void
	onClose?: () => void
	onFocus?: () => void
	isFocused: boolean
	reservedY: number
	width?: number
	height?: number
	// resizeable?: boolean // unused
	minimized?: boolean
}

type WindowContentExports = {
	closeWindow: () => void
}
export type DraggableWindowChildren = ComponentChildren | ((props: WindowContentExports) => ComponentChildren)

const DraggableWindow = (props: DraggableWindowProps) => {
	const [dragging, setDragging] = useState(false)
	const [x, setX] = useState(16)
	const [y, setY] = useState(16)
	const [offsetX, setOffsetX] = useState(0)
	const [offsetY, setOffsetY] = useState(0)
	const [width, setWidth] = useState<number | undefined>(props.width)
	const [height, setHeight] = useState<number | undefined>(props.height)
	const [maximized, setMaximized] = useState(false)
	const [lastClick, setLastClick] = useState(new Date())
	const windowDiv = useRef<HTMLDivElement>(null)

	useEffect(() => {
		const mouseMoveListener = (e: MouseEvent) => {
			if (dragging) {
				setX(e.x + offsetX)
				setY(e.y + offsetY)
			}
		}
		window.addEventListener("mousemove", mouseMoveListener)
		return () => {
			window.removeEventListener("mousemove", mouseMoveListener)
		}
	}, [dragging, offsetX])

	const effX = () => (maximized ? 0 : x)
	const effY = () => (maximized ? 0 : y)
	const effWidth = () => (maximized ? window.innerWidth : width)
	const effHeight = () => (maximized ? window.innerHeight - props.reservedY : height)

	useEffect(() => {
		if (!windowDiv.current) return
		if (!width) setWidth(windowDiv.current.scrollWidth + 2)
		if (!height) setHeight(windowDiv.current.scrollHeight)
	}, [windowDiv, width, height])

	return (
		<div
			className={classNames(styles.window, maximized && styles.maximized, { hidden: props.minimized, flex: !props.minimized })}
			style={{
				position: "absolute",
				top: `${effY()}px`,
				left: `${effX()}px`,
				width: `${effWidth()}px`,
				height: `${effHeight()}px`,
				zIndex: props.isFocused ? 15 : 0,
			}}
			onMouseDown={() => props.onFocus && props.onFocus()}
			ref={windowDiv}
		>
			<div
				className={styles.titlebar}
				onMouseDown={(e) => {
					if (maximized || (e.target as HTMLElement).nodeName == "BUTTON") return
					setDragging(true)
					setOffsetX(x - e.x)
					setOffsetY(y - e.y)
				}}
				onMouseUp={() => {
					setDragging(false)
				}}
				onClick={() => {
					setLastClick(new Date())
					if (new Date().getTime() - lastClick.getTime() < 200) setMaximized(!maximized)
				}}
			>
				<span>{props.title}</span>
				<div className={styles.titlebuttons}>
					<button onClick={() => props.onMinimize && props.onMinimize()}>
						<MinusIcon />
					</button>
					<button onClick={() => setMaximized(!maximized)}>{maximized ? <ArrowsPointingInIcon /> : <ArrowsPointingOutIcon />}</button>
					<button className={styles.close} onClick={() => props.onClose && props.onClose()}>
						<XMarkIcon />
					</button>
				</div>
			</div>
			<div className={styles.content} style={{ height: `${(effHeight() || 0) - 43}px` }}>
				<WindowErrorBoundary>
					{typeof props.children === "function" ? <props.children closeWindow={() => props.onClose && props.onClose()} /> : props.children}
				</WindowErrorBoundary>
			</div>
		</div>
	)
}

export default DraggableWindow

import { ComponentChildren } from "preact"
import { useErrorBoundary } from "preact/hooks"

export const WindowErrorBoundary = (props: { children: ComponentChildren }) => {
	const [error] = useErrorBoundary()

	if (error)
		return (
			<div className="p-4">
				<p className="font-semibold">This app has ran into a problem and needs to be restarted.</p>
				<pre className="whitespace-pre-wrap pt-2 pb-4 text-xs">{error.stack}</pre>
			</div>
		)

	return <>{props.children}</>
}

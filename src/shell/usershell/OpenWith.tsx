import { useId, useState } from "preact/hooks"

const filenameRegex = /(?:\/\w+)*\/(.+)/

export const OpenWith = (props: { path: string; extension?: string; closeWindow: () => void }) => {
	const [showAll, setShowAll] = useState(false)
	const [defaultApp, setDefaultApp] = RealSystem.user.useRegistryState<string | undefined>(
		`System/FileAssociations/Extensions/${props.extension}`
	)
	const [asDefault, setAsDefault] = useState(!defaultApp)
	const inputID = useId()
	const input2ID = useId()
	const apps =
		props.extension && !showAll
			? RealSystem.kernel.getApps().filter((app) => app.extensions?.includes(props.extension as string))
			: RealSystem.kernel.getApps()

	return (
		<div className="overflow-auto">
			<p className="p-4">What app do you want to open this file with? ({(filenameRegex.exec(props.path) || [])[1]})</p>
			{apps.map((app) => (
				<button
					className="block w-full border-t border-black/20 py-4 px-6 text-left transition-all last-of-type:border-b hover:bg-slate-200"
					key={app.executable}
					onClick={() => {
						RealSystem.kernel.start(app.executable, [props.path])
						if (asDefault) {
							setDefaultApp(app.executable)
						}
						props.closeWindow()
					}}
				>
					<span className="font-semibold">{app.name}</span> (by {app.author})
				</button>
			))}
			<div className="p-4">
				<div>
					<input id={inputID} type="checkbox" onChange={(e) => setShowAll(e.currentTarget.checked)} checked={showAll} />
					<label className="ml-2" for={inputID}>
						Show all apps
					</label>
				</div>
				{props.extension && (
					<div>
						<input id={input2ID} type="checkbox" onChange={(e) => setAsDefault(e.currentTarget.checked)} checked={asDefault} />
						<label className="ml-2" for={input2ID}>
							Set as default for this type of file (.{props.extension})
						</label>
					</div>
				)}
			</div>
		</div>
	)
}

import { LoginEvent } from "@/kernel"
import { OOBEMain } from "@/shell/oobe/OOBEMain"
import { RSLogonMain } from "@/shell/rslogon/RSLogonMain"
import { WindowManager } from "@/shell/usershell"
import { DraggableWindowChildren } from "@/shell/usershell/DraggableWindow"
import { useEffect, useState } from "preact/hooks"

export type OpenWindow = {
	title: string
	children: DraggableWindowChildren
	width?: number
	height?: number
	minimized?: boolean
	openedAt?: Date
}

type ShellExports = {
	openWindow: (window: OpenWindow) => void
	openWithApp: (path: string, extension?: string) => void
}

export const stubShellExports: ShellExports = {
	openWindow: () => {},
	openWithApp: () => {},
}

export const ShellMain = () => {
	const [oobeDone] = RealSystem.user.useRegistryState("System/Setup/Done")
	const [currentUser, setCurrentUser] = useState<string>()
	useEffect(() => {
		const listener = (e: LoginEvent) => {
			setCurrentUser(e.username)
		}
		window.addEventListener("realsystem.login", listener as (e: Event) => void)

		return () => {
			window.removeEventListener("realsystem.login", listener as (e: Event) => void)
		}
	}, [])

	if (!oobeDone) return <OOBEMain />
	if (!currentUser) return <RSLogonMain />
	return <WindowManager user={currentUser} />
}

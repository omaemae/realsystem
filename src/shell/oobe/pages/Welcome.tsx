import { OOBEPage } from "@/shell/oobe/pages"
import { ArrowLeftOnRectangleIcon } from "@heroicons/react/24/solid"

export const Welcome: OOBEPage = ({ next }) => (
	<div className="flex h-full flex-col justify-between">
		<div>
			<h1 className="text-center text-3xl font-semibold">
				Welcome to{" "}
				<span className="bg-gradient-to-r from-red-900 to-red-700 bg-clip-text text-3xl font-bold text-transparent">RealSystem</span>
			</h1>
			<p className="py-4">This setup wizard will walk you through setting up your RealSystem installation.</p>
		</div>

		<div className="flex justify-center">
			<RealSystem.ui.Button onClick={() => next()}>
				<ArrowLeftOnRectangleIcon />
				Begin
			</RealSystem.ui.Button>
		</div>
	</div>
)

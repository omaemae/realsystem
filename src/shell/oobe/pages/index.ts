import { JSX } from "preact"

export type OOBEPage = (props: { next: () => void }) => JSX.Element

import { OOBEPage } from "@/shell/oobe/pages"
import { UserPlusIcon } from "@heroicons/react/24/solid"
import { useState } from "preact/hooks"

export const User: OOBEPage = ({ next }) => {
	const [username, setUsername] = useState("")
	const [error, setError] = useState<string | null>(null)

	return (
		<div className="flex h-full flex-col gap-4">
			<div className="flex-1">
				<h1 className="text-2xl font-semibold">Create a user</h1>
				<p>create a user so you can do shit</p>
			</div>
			<form
				className="flex flex-col items-start gap-2"
				onSubmit={(e) => {
					e.preventDefault()
					RealSystem.kernel
						.createUser(username)
						.then(() => next())
						.catch(() => setError("you bitch"))
				}}
			>
				<p className="text-slate-700">Username:</p>
				<RealSystem.ui.Input type="text" value={username} onChange={(e) => setUsername(e.currentTarget.value)} />
				<RealSystem.ui.Button type="submit">
					<UserPlusIcon />
					Create a User
				</RealSystem.ui.Button>
			</form>
			{error && <p className="text-4xl font-semibold text-red-700">{error}</p>}
		</div>
	)
}

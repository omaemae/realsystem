import { readFileAsBlob } from "@/internal/helpers"
import styles from "@/shell/oobe/OOBEMain.module.css"
import { User } from "@/shell/oobe/pages/User"
import { Welcome } from "@/shell/oobe/pages/Welcome"
import { useEffect, useState } from "preact/hooks"

const steps = [Welcome, User]

export const OOBEMain = () => {
	const [bgBlob, setBGBlob] = useState("")
	const [stage, setStage] = RealSystem.user.useRegistryState<number>("System/Setup/Stage")
	const [_, setDone] = RealSystem.user.useRegistryState<boolean>("System/Setup/Done")

	const CurrentPage = steps[stage]

	useEffect(() => {
		readFileAsBlob("/usr/share/backgrounds/realsystem.png").then((blob) => setBGBlob(blob))
	}, [])

	const nextPage = () => {
		const nextStage = stage + 1
		if (nextStage >= steps.length) {
			setDone(true)
		} else {
			setStage(nextStage)
		}
	}

	return (
		<div className={styles.bg} style={{ backgroundImage: `url(${bgBlob})` }}>
			<div className={styles.oobeContainer}>
				<CurrentPage next={() => nextPage()} />
			</div>
		</div>
	)
}

const jsx = RealSystem.libraries.preactJSX.jsx

const ExampleWindow = () => jsx("p", { children: "Hello world!", style: { padding: "1rem" } })

class ExampleApp {
	constructor() {
		this.name = "Example App"
		this.author = "omame"
		this.executable = "exampleapp"
	}

	main() {
		RealSystem.shell.openWindow({
			title: this.name,
			children: jsx(ExampleWindow, {}),
		})
	}
}

export default ExampleApp

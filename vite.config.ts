import { defineConfig } from "vite"
import preact from "@preact/preset-vite"
import { resolve } from "path"
import { execSync } from "child_process"

const commitHash = execSync("git rev-parse --short HEAD").toString().trim()
const commitCount = execSync("git rev-list --count HEAD").toString().trim()

export default defineConfig({
	plugins: [preact()],
	resolve: {
		alias: {
			"@": resolve(__dirname, "./src"),
			"@rootfs": resolve(__dirname, "./rootfs"),
		},
	},
	define: {
		COMMIT_HASH: JSON.stringify(commitHash),
		COMMIT_COUNT: JSON.stringify(commitCount),
	},
})

/** @type {import("tailwindcss").Config} */
module.exports = {
	content: ["./src/**/*.tsx"],
	theme: {
		extend: {
			fontFamily: {
				sans: ["InterVariable", "Inter", "Arial", "sans-serif"],
				mono: ["'JetBrains Mono'", "monospace"],
			},
			animation: {
				windowOpen: "windowOpen 0.3s cubic-bezier(0.4, 0, 0.2, 1)",
			},
			keyframes: {
				windowOpen: {
					"0%": {
						transform: "translateY(1rem) scale(80%)",
						opacity: 0,
					},
					"100%": {
						transform: "translateY(0rem) scale(100%)",
						opacity: 1,
					},
				},
			},
		},
	},
	plugins: [],
}
